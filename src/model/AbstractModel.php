<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Example\model;

use Silex\Application;

/**
 * Class AbstractModel
 * @package worldsailing\Example\model
 */
Abstract class AbstractModel {

    /**
     * @var Application
     */
    protected $app;

    /**
     * AbstractModel constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }
}
