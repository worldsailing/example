<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Example\model\entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


use JsonSerializable;

/**
 * @ORM\Entity
 */
class Example implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $BiogMembId;
    /**
     * @ORM\Column(type="string", length=30)
     */
    protected $BiogIsafId;
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $BiogFirstName;
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $BiogSurname;
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $BiogEmail;
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $CreatedAt;
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $UpdatedAt;

    /**
     * @return array
     */
    protected static function getDefaultValues()
    {
        return [
            'CreatedAt' => null,
            'UpdatedAt' => null
        ];
    }

    public static function getConstraints()
    {
        $constraints = new Assert\Collection([
            'fields' => [
                'BiogIsafId' => [new Assert\NotBlank(), new Assert\Length(['min' => 5])],
                'BiogFirstName' => [new Assert\NotBlank(), new Assert\Length(['min' => 5])],
                'BiogSurname' => [new Assert\NotBlank(), new Assert\Length(['min' => 5])],
                'BiogEmail' => [new Assert\NotBlank(), new Assert\Email()]
            ],
            'allowExtraFields' => true
        ]);

        return $constraints;
    }


    /**
     * @return mixed
     */
    public function getBiogMembId()
    {
        return $this->BiogMembId;
    }

    /**
     * @param mixed $BiogMembId
     * @return Biog
     */
    public function setBiogMembId($BiogMembId)
    {
        $this->BiogMembId = $BiogMembId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBiogIsafId()
    {
        return $this->BiogIsafId;
    }

    /**
     * @param mixed $BiogIsafId
     * @return Biog
     */
    public function setBiogIsafId($BiogIsafId)
    {
        $this->BiogIsafId = $BiogIsafId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBiogFirstName()
    {
        return $this->BiogFirstName;
    }

    /**
     * @param mixed $BiogFirstName
     * @return Biog
     */
    public function setBiogFirstName($BiogFirstName)
    {
        $this->BiogFirstName = $BiogFirstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBiogSurname()
    {
        return $this->BiogSurname;
    }

    /**
     * @param mixed $BiogSurname
     * @return Biog
     */
    public function setBiogSurname($BiogSurname)
    {
        $this->BiogSurname = $BiogSurname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBiogEmail()
    {
        return $this->BiogEmail;
    }

    /**
     * @param mixed $BiogEmail
     * @return Biog
     */
    public function setBiogEmail($BiogEmail)
    {
        $this->BiogEmail = $BiogEmail;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->CreatedAt;
    }

    /**
     * @param mixed $CreatedAt
     * @return Example
     */
    public function setCreatedAt($CreatedAt)
    {
        $this->CreatedAt = $CreatedAt;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->UpdatedAt;
    }

    /**
     * @param mixed $UpdatedAt
     * @return Biog
     */
    public function setUpdatedAt($UpdatedAt)
    {
        $this->UpdatedAt = $UpdatedAt;
        return $this;
    }

    /**
     * @param $key
     * @return string|null
     */
    public function getDefaultValue($key)
    {
        $defaults = self::getDefaultValues();
        if (isset($defaults[$key])) {
            return $defaults[$key];
        } else {
            return null;
        }
    }

    /**
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        foreach (get_object_vars($this) as $key => $prop) {
            if (isset($data[$key])) {
                if ($data[$key] == '' || $data[$key] === null) {
                    $this->{$key} = $this->getDefaultValue($key);
                } else {
                    $this->{$key} = $data[$key];
                }
            }
        }
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
