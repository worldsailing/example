<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Example\model;

use worldsailing\Example\model\entity\Example;
use Doctrine\Common\Collections\Criteria;

/**
 * Class ExampleModel
 * @package worldsailing\model
 */
class ExampleModel extends AbstractModel {

    /**
     * @var  \Symfony\Component\Validator\ConstraintViolationListInterface[]
     */
    private $validationErrors = [];

    /**
     * ExampleModel constructor.
     * @param \Silex\Application $app
     */
    public function __construct($app)
    {
        parent::__construct($app);
    }

    /**
     * @param $data
     * @return bool
     */
    public function validate($data)
    {
        $constraints = Example::getConstraints();

        $this->validationErrors = $this->app['validator']->validate($data, $constraints);
        if (count($this->validationErrors) >0) {
            foreach ($this->validationErrors as $error)
                $this->app['monolog']->addDebug($error);
        }
        return count($this->validationErrors) == 0;
    }

    /**
     * @return \Symfony\Component\Validator\ConstraintViolationListInterface[]
     */
    public function getValidationErrors()
    {
        return $this->validationErrors;
    }

    /**
     * @return string
     */
    public function getValidationMessages()
    {
        $message = '';
        /** @var \Symfony\Component\Validator\ConstraintViolationInterface $error */
        foreach ($this->validationErrors as $error) {

            $message .= $error->getPropertyPath().' '.$error->getMessage()."\n";
        }
        return $message;
    }

    /**
     * @param array $filters
     * @param null $limit
     * @param null $offset
     * @param array $order
     * @return Example[]
     */
    public function findAll($filters = [], $limit = null, $offset = null, $order = [])
    {
        return $this->app['orm.ems']['example']->getRepository('worldsailing\Example\model\entity\Example')->findBy($filters, $order, $limit, $offset);
    }

    /**
     * @param array $filters
     * @return int
     */
    public function countAll($filters = [])
    {
        return count($this->app['orm.ems']['example']->getRepository('worldsailing\Example\model\entity\Example')->findBy($filters));
    }


    /**
     * @param $id
     * @return Example
     */
    public function findById($id)
    {
        return $this->app['orm.ems']['example']->find('worldsailing\Example\model\entity\Example', $id);
    }


    /**
     * @param \Doctrine\Common\Collections\Criteria $criteria
     * @return \Doctrine\Common\Collections\Collection
     */
    public function findAllByCriteria( Criteria $criteria)
    {
        return $this->app['orm.ems']['example']->getRepository('worldsailing\Example\model\entity\Example')->matching($criteria);
    }


    /**
     * @param Criteria $criteria
     * @return int
     */
    public function countAllByCriteria( Criteria $criteria)
    {
        return count($this->app['orm.ems']['example']->getRepository('worldsailing\Example\model\entity\Example')->matching($criteria));
    }


    /**
     * @param $data
     * @return bool
     */
    public function setEntityData( $data )
    {
        $entity = $this->app['orm.ems']['example']->find('worldsailing\Example\model\entity\Example', $data['BiogMembId']);

        $entity->setData($data);
        if ($data['BiogMembId'] != 0) {
            $entity->setUpdatedAt(date('Y-m-d H:i:s'));
        }

        try {
            $this->app['orm.ems']['example']->merge($entity);
            $this->app['orm.ems']['example']->flush();
        } catch (\Exception $e) {
            $this->app['monolog']->addError($e->getMessage());
            return false;
        }
        return true;
    }


}
