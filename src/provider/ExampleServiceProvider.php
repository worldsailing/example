<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Example\provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use worldsailing\Example\service\ExampleService;

use Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider;


/**
 * Class ExampleServiceProvider
 * @package worldsailing\Example\provider
 */
class ExampleServiceProvider implements ServiceProviderInterface
{

    /**
     * @param Container $app
     */
    public function register(Container $app)
    {
        /*
         * Register Doctrine ORM for affected tables
         */
        if (! isset($app['orm.ems']) ) {
            $app->register(new DoctrineOrmServiceProvider, [
                'orm.ems.options' => [
                    'example' => [
                        'connection' => "isaf",
                        'auto_generate_proxy_classes' => "%kernel.debug%",
                        'auto_mapping' => false,
                        'mappings' => [
                            [
                                'type' => "annotation",
                                'namespace' => "worldsailing\\Example\\model\\entity\\Example",
                                'path' => __DIR__ . "/../model/entity/Example",
                                'use_simple_annotation_reader' => false //if this is true OR missing then use @Entity on Doctrine entity rather than @ORM\Entity
                            ]
                        ]
                    ],
                ]
            ]);
        }

        $app['service.example'] = function ($app) {
            return new ExampleService($app);
        };

        return;
    }
}
