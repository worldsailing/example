<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Example\service;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use worldsailing\Api\response\ValidationErrorList;
use worldsailing\Example\model\ExampleModel;
use Doctrine\Common\Collections\Criteria;
use worldsailing\Helper\UrlHelper;
use worldsailing\Common\BundleResultSet\CompositeListResultSet;
use worldsailing\Common\BundleResultSet\CompositeEntityResultSet;
use worldsailing\Common\Exception\WsServiceException;

/**
 * Class ExampleService
 * @package worldsailing\Example\service
 */
Class ExampleService {

    /**
     * @var \Silex\Application
     */
    protected $app;

    /**
     * ExampleService constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param Application $app
     * @return ExampleModel
     */
    public function modelFactory(Application $app)
    {
        return new ExampleModel($app);
    }

    /**
     * @param ExampleModel $model
     * @param Request|null $request
     * @return CompositeListResultSet
     */
    public function getExampleList(ExampleModel $model, Request $request = null)
    {
        if ($request === null) {
            $request = Request::createFromGlobals();
        }

        $limit = UrlHelper::getLimitOfRequest($request);

        $offset = UrlHelper::getOffsetOfRequest($request);

        $order = UrlHelper::getOrderOfRequest($request);

        $criteria = new Criteria();
        $criteria->where($criteria::expr()->neq('BiogIsafId', ''));

        $count = $model->countAllByCriteria($criteria);

        if (count($order) > 0) {
            $criteria->orderBy($order);
        }

        if ($limit > 0) {
            $criteria->setMaxResults($limit)
                ->setFirstResult($offset);
        }

        $data = $model->findAllByCriteria($criteria)->toArray();

        return (new CompositeListResultSet($data))->setCountAll($count);

    }


    /**
     * @param ExampleModel $model
     * @param $id
     * @return CompositeEntityResultSet
     */
    public function getExampleById(ExampleModel $model, $id)
    {
        return (new CompositeEntityResultSet($model->findById($id)));
    }


    /**
     * @param ExampleModel $model
     * @param Request $request
     * @param $id
     * @return CompositeEntityResultSet
     * @throws WsServiceException
     * @throws \Exception
     */
    public function setExampleData(ExampleModel $model, Request $request, $id) {

        $this->app['monolog']->addDebug($request->__toString());


        if ($model->validate($request->query->all())) { //validation
            if ($model->setEntityData($request->query->all())) {
                return $this->getExampleById($model, $id);
            } else {
                throw new \Exception('An Error Occurred', 500);
            }
        } else {
            $e = new WsServiceException('Validation error', 400);
            /**
             * \Symfony\Component\Validator\ConstraintViolationListInterface
             */
            $errors = $model->getValidationErrors();
            /**
             * This line creates a standard API Validation Error List format
             *
             * You can read that by $e->__map() as array or $e->__json() as string
             */
            $e->__setData((new ValidationErrorList('errors', $errors)));
            throw $e;
        }
    }
}

