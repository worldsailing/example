# worldsailing Example 

### PHP library description
Example bundle to World Sailing microservices based on Silex.

  
### Install by composer
```php
{
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://worldsailing@bitbucket.org/worldsailing/example.git"
    }
  ],
  "require": {
    "worldsailing/example": "dev-master"
  }
}
```
 
### License

All right reserved
